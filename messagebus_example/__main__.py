"""Messagebus example main entrypoint."""

from cki_lib import logger
from cki_lib import messagequeue

LOGGER = logger.get_logger('messagebus_example')


def main():
    """Print routing keys of messages flying by."""
    messagequeue.MessageQueue().consume_messages(
        'cki.exchange.webhooks', ['#'],
        lambda routing_key=None, **_: LOGGER.warning('%s', routing_key),
        queue_name="cki.queue.messagebus-example",
    )


if __name__ == '__main__':
    main()
