# messagebus example

## Usage

Install dependencies in a suitable way for you environment, e.g. for a venv do

```shell
pip install -e .
```

Configure environment variables in .env:

```shell
export RABBITMQ_HOST="host1 host2 host3"
export RABBITMQ_PORT="5671"
export RABBITMQ_CAFILE="rh-cki.crt"
export RABBITMQ_VIRTUAL_HOST="/cki-staging"
export RABBITMQ_USER="user name"
export RABBITMQ_PASSWORD="pass word"
```

Import the configuration into you current shell session:

```shell
. .env
```

Start the listener:

```shell
python3 -m messagebus_example
```

This will use a temporary queue. In staging or production mode, a persistent
queue would be used.
